import sys

def fuerza_bruta(file):
    with open(file, 'rb') as f:
        bin = f.read()

    aux = ''
    for i in range(0, 256):
        aux = ''
        for ch in bin:
            aux += str(hex(ch ^ i))
        fi = aux.find('0x540x680x690x730x200x700x720x6f0x670x720x610x6d0x200x630x610x6e0x6e0x6f0x740x200x620x650x200x720x750x6e0x200x690x6e0x200x440x4f0x530x200x6d0x6f0x640x65')
        if fi != -1:
            return i
        
def descifrar(file, key):
    with open(file, 'rb') as f:
        bin = f.read()
    aux = []
    for ch in bin:
        aux.append(hex(ch ^ key))
    return aux
    

if __name__ == '__main__':
    if(len(sys.argv) != 3):
        print("Debes pasar 2 parámetros de la siguiente forma")
        print("python 57FD6325.VBN malware.bin")
        print("El primero será el archivo a leer y el segundo el archivo de salida")
        sys.exit(-1)
    file = sys.argv[1]
    write = sys.argv[2]
    print("Encontrando la llave de 1 byte por fuerza bruta...")
    key = fuerza_bruta(file)
    print("La llave es: " + str(key))
    print("Descifrando el archivo con la llave encontrada y la operación XOR...")
    to_write = descifrar(file, key)
    with open(write, 'w') as f:
        for word in to_write:
            f.write(chr(int(word, 16)))
    print("Se finalizó el proceso y se guardó en el archivo: " + write)